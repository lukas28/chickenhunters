﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class MarkerNotTracked : MonoBehaviour, ITrackableEventHandler
{
  /*
  declaring the neccessary variables
  */
  private TrackableBehaviour mTrackableBehaviour;
  public Camera mainCamera;

  /*
  initialize the variables (used to get the information whether a marker is
  currently tracked or not)
  */
  void Start()
  {
    mTrackableBehaviour = GetComponent<TrackableBehaviour>();
    if (mTrackableBehaviour)
    {
        mTrackableBehaviour.RegisterTrackableEventHandler(this);
    }
  }

  /*
  This callback is called if the tracking situation changes. In this case this
  is used to change the culling layers of the camera. Since some GameObjects are
  initialized over the network, they are not children of the image target. By
  getting notified that the corresponding marker is not tracked any more, all this
  components are removed from culling and added again if the marker is tracked again.
  */
  public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus,TrackableBehaviour.Status newStatus)
  {
      if (newStatus == TrackableBehaviour.Status.DETECTED ||
          newStatus == TrackableBehaviour.Status.TRACKED ||
          newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
      {
          Debug.Log("Visible again");
          mainCamera.cullingMask = -1;
      }	else
      {
        Debug.Log("NOT VISIBLE");

        GameObject[] chicken = GameObject.FindGameObjectsWithTag("Respawn");
        Debug.Log("found " + chicken.Length);
        mainCamera.cullingMask = (1 << LayerMask.NameToLayer("Default"));

      }
    }
}
