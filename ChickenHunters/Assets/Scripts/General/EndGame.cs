using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour
{

    public GameObject endButton;

    // Start is called before the first frame update
    void Start()
    {

    }

    /*
    This method is called from the winner and loser result scene and initializes
    the loading of the menu scene.
    */
    public void End()
    {
        SceneManager.LoadScene("Menu");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
