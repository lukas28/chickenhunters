using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;
using Photon.Pun;
using Photon.Realtime;
using System.IO;
using UnityEngine.SceneManagement;
using Photon.Pun.UtilityScripts;
using System.Threading;

public class PhotonRoom : MonoBehaviourPunCallbacks, IInRoomCallbacks
{

    /*
    Declare all the neccessary variables
    */
    public static PhotonRoom room;
    public PhotonView PV;
    public bool isGameLoaded;
    public int currentScene;

    private Player[] photonPlayers;
    public int playersInRoom;
    public int myNumberInRoom;

    public int playerInGame;

    private bool readyToCount;
    private bool readyToStart;
    public float startingTime;
    private float lessThanMaxPlayers;
    private float atMaxPlayer;
    private float timeToStart;

    private bool sceneryCreated;
    private Vector3 spawnPosition;
    private int currentSpawnId;
    private int currentSpawnedCount;

    private int objectToDestroy;

    GameObject scoreTextField;

    private int timeLeft=30;
    public string winner="";
    private bool gameFinished = false;
    private int scoreLimit = 20;
    private int personalScore = 0;
    private int maxChickenCount = 10;

    //here it is about the chicken spawning
    private int nrOfChicken;
    private bool gameInitialized;

    public List<GameObject> objectIds = new List<GameObject>();

    /*
    Create a singelton object of this PhotonRoom object
    */
    void Awake(){
        if (PhotonRoom.room == null){
          PhotonRoom.room = this;
        }else{
          if(PhotonRoom.room != this){
            Destroy(PhotonRoom.room.gameObject);
            PhotonRoom.room = this;
          }
        }
        DontDestroyOnLoad(this.gameObject);
    }

    /*
    Initialize this PhotonRoom object as the callback target for the PhotonNetwork.
    Register the OnSceneFinishedLoading method as callback if the SceneManager
    successfully loaded a new game scene
    */
    public override void OnEnable(){
      base.OnEnable();
      PhotonNetwork.AddCallbackTarget(this);
      SceneManager.sceneLoaded += OnSceneFinishedLoading;

    }

    /*
    Counterpart to the OnEnable function. It removes the callback target of the
    PhotonNetwork from this PhotonRoom object and removes also the OnSceneFinishedLoading
    callback from the scene manager.
    */
    public override void OnDisable(){
      base.OnDisable();
      PhotonNetwork.RemoveCallbackTarget(this);
      SceneManager.sceneLoaded -= OnSceneFinishedLoading;
    }

    /*
    When this script starts, all the neccessary variables are set.
    */
    void Start(){
        PV = GetComponent<PhotonView>();
        readyToCount = false;
        readyToStart = false;
        sceneryCreated = false;
        lessThanMaxPlayers = startingTime;
        atMaxPlayer = 15;
        currentSpawnedCount = 0;
        timeToStart = startingTime;
        nrOfChicken = 0;
        gameInitialized = false;
        timeLeft = 60;
        Time.timeScale = 1;
    }

    /*
    This method is used when the game is over. It Desconnects the player from
    the PhotonRoom and the PhotonNetwork.
    */
    void endGame(bool winner){
      Debug.Log("now ending game");
      DisconnectPlayer(winner);
    }

    /*
    In the update method, at first the application is constantly checking if
    there are already all users connected so the game could start. and if so the
    game is started. Furthermore, if the game is going on, the ui element showing
    the current scores is updated. Another function in the update method is to
    respawn chicken if there are less than 10 available - this is only done by the
    master client.
    */
    void Update()
    {
      if(!gameFinished)
      {
        if(MultiplayerSettings.multiplayerSetting.delayStart)
        {
          if(playersInRoom == 1)
          {
            RestartTimer();
          }
          if(!isGameLoaded)
          {
            if(readyToStart == true)
            {
              atMaxPlayer -= Time.deltaTime;
              lessThanMaxPlayers = atMaxPlayer;
              timeToStart = atMaxPlayer;
            }
            else if(readyToCount)
            {
              lessThanMaxPlayers -= Time.deltaTime;
              timeToStart = lessThanMaxPlayers;
            }
            if(timeToStart <= 0)
            {
              StartGame();
            }
          }else
          {
            // Game is already going on, in the update method update the current score
            updateScore();
          }
        }

        if (PhotonNetwork.IsMasterClient && nrOfChicken < maxChickenCount && gameInitialized)
        {
          PV.RPC("RPC_SpawnChicken", RpcTarget.All);
          nrOfChicken++;
        }
      }
    }

    /*
    Get the players local ScoreText UI element and the actual scores of the
    players and set the score.
    Returns whether a player is already finished and the game should be stopped.
    */
    private void updateScore()
    {
      GameObject scoreTextField = GameObject.Find("ScoreText");
      if (scoreTextField != null)
      {
        Text scoreTextFieldText = scoreTextField.GetComponent<Text>();
        string scoreText = "";

        foreach (Player pl in photonPlayers)
        {
          if(string.Equals(pl.NickName, PhotonNetwork.LocalPlayer.NickName))
          {
            scoreText = scoreText + "You" + " - " + pl.GetScore() + "\n";
          }
          else
          {
              scoreText = scoreText + "Other" + " - " + pl.GetScore() + "\n";
          }

        }
        scoreTextFieldText.text = scoreText;
      }
    }

    /*
    Method is used to reset the score of all players in the game to 0.
    */
    void resetScore()
    {
      foreach (Player pl in photonPlayers)
      {
        pl.SetScore(0);
      }
    }

    /*
    Getting the callback if one player left the game.
    */
    public void OnPhotonPlayerDisconnected()
    {
      Debug.Log("player left game");
    }

    /*
    Getting the callback that a a player entered the room. If the required amount
    of players is now connected to the room, the game can be started.
    */
    public override void OnJoinedRoom()
    {
      base.OnJoinedRoom();
      Debug.Log("We are now in a room");
      photonPlayers = PhotonNetwork.PlayerList;
      playersInRoom = photonPlayers.Length;
      myNumberInRoom = playersInRoom;
      PhotonNetwork.NickName = myNumberInRoom.ToString();
      if(MultiplayerSettings.multiplayerSetting.delayStart)
      {
        Debug.Log("Displayer players in room out of max Players possible");
        if(playersInRoom > 1)
        {
          readyToCount = true;
        }
        if(playersInRoom == MultiplayerSettings.multiplayerSetting.maxPlayers)
        {
          readyToStart = true;
          if(!PhotonNetwork.IsMasterClient)
          {
            return;
          }
          PhotonNetwork.CurrentRoom.IsOpen = false;
        }
      }
      else
      {
        StartGame();
      }
    }


    /*
    Getting the callback that the player who joined the room now entered the room.
    If the required amount of players are now in the room, the room is called closed
    so no more players could connect and the game can be started.
    */
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
      base.OnPlayerEnteredRoom(newPlayer);
      Debug.Log("A new player entered the room");
      photonPlayers = PhotonNetwork.PlayerList;
      playersInRoom++;

      if(MultiplayerSettings.multiplayerSetting.delayStart)
      {
        if(playersInRoom > 1)
        {
          readyToCount = true;
        }
        if(playersInRoom == MultiplayerSettings.multiplayerSetting.maxPlayers)
        {
          readyToStart = true;
          if(!PhotonNetwork.IsMasterClient)
          {
            return;
          }
          PhotonNetwork.CurrentRoom.IsOpen = false;
        }
      }
    }

    /*
    Getting the callback that a player left the room. Since this could be the case
    because one of the players already ended the game and due to the network lag this
    player is still in the game, the logic chacks if the game is maybe over.
    */
    public override void OnPlayerLeftRoom(Player player)
    {
      Debug.Log("players still in room: " + PhotonNetwork.PlayerList.Length);
      if(PhotonNetwork.PlayerList.Length == 1)
      {
        testEnd(false);
      }

    }

    /*
    This method is called to start the game which means there are all required
    players connected and ready. If so the game scene is laoded and the players are
    forwareded into that scene to start the game.
    */
    void StartGame()
    {
      resetScore();
      Debug.Log("Started Game");
      isGameLoaded = true;
      if(!PhotonNetwork.IsMasterClient)
      {
        return;
      }
      if(MultiplayerSettings.multiplayerSetting.delayStart)
      {
        PhotonNetwork.CurrentRoom.IsOpen = false;
      }
      PhotonNetwork.LoadLevel(MultiplayerSettings.multiplayerSetting.multiplayerScene);
      PhotonNetwork.AutomaticallySyncScene = false;
    }

    /*
    This restarts the connection timer which periodically checks if there are
    all neccessary players connected.
    */
    void RestartTimer()
    {
      lessThanMaxPlayers = startingTime;
      timeToStart = startingTime;
      atMaxPlayer = 2;
      readyToCount = false;
      readyToStart = false;
      sceneryCreated = false;
    }

    /*
    Getting this callback if the game scene is loaded. If the local client
    is the master client, he is responsible for spawining the chicken.
    */
    void OnSceneFinishedLoading(Scene scene, LoadSceneMode mode)
    {
      if(!PhotonNetwork.IsMasterClient)
      {
        Debug.Log("Ich bin das Handy und versuche einen Stone player zu adden");
      }
      else
      {
        Debug.Log("Ich bin der Laptop und versuche einen Stone player zu adden");
        Debug.Log(scene.buildIndex);
      }

      currentScene = scene.buildIndex;
      if(currentScene == MultiplayerSettings.multiplayerSetting.multiplayerScene)
      {
        Debug.Log("wieder hier");
        isGameLoaded = true;
        if(MultiplayerSettings.multiplayerSetting.delayStart)
        {
          PV.RPC("RPC_LoadedGameScene", RpcTarget.MasterClient);
          if(isGameLoaded && nrOfChicken < 5 && PhotonNetwork.IsMasterClient)
          {
              PV.RPC("RPC_SpawnChicken", RpcTarget.All);
              nrOfChicken++;
              Debug.Log(nrOfChicken);
              gameInitialized = true;
          }
        }
      }
      else if(currentScene == 3 || currentScene == 4)
      {
        if(PhotonNetwork.IsMasterClient)
        {
          Debug.Log("should have disconneted");
           PhotonNetwork.Disconnect();
        }
      }
    }


    /*
    This method is called as remote procedure call if one player shot the grass.
    A appropriate animation is initialized over the network so that all players
    can see the impact.
    */
    [PunRPC]
    public void hitGrass(Vector3 impactPosition)
    {
      PhotonNetwork.InstantiateSceneObject(Path.Combine("PhotonPrefabs", "PhotonNetworkDirtBulletEffect"), impactPosition, Quaternion.identity, 0);
    }

    /*
    This remote procedure call function is used if a player hit a chicken and the
    master client should destroy the chicken to remove it from the network. also the
    score should adapted
    */
    [PunRPC]
    public void hitChicken(int obj, string nickname)
    {
      if(PhotonNetwork.IsMasterClient)
      {
        Debug.Log("HIT CHICKEN in: " + PhotonNetwork.LocalPlayer.NickName);
        objectToDestroy = obj;
        destroyChicken(nickname);
        Debug.Log("after notifygameended");
      }
      else
      {
        Debug.Log("HIT CHICKEN in: " + PhotonNetwork.LocalPlayer.NickName);
        //PV.RPC("notifyGameEnded", RpcTarget.All, nickname);
        notifyGameEnded(nickname);
        Debug.Log("after notifygameended non master");
      }

    }

    /*
    This method is called if the game is over. It checks whether the local
    player won or not and based on that the winner result scene or the looser
    result scene is loaded.
    */
    void testEnd(bool winner)
    {
      Debug.Log("ending the game now");
      string myName = PhotonNetwork.NickName;
      gameFinished = true;

      if(winner)
      {
        SceneManager.LoadScene(MultiplayerSettings.multiplayerSetting.winnerScene);
        PhotonNetwork.DestroyPlayerObjects(PhotonNetwork.LocalPlayer);
        PhotonNetwork.LeaveRoom();
      }
      else
      {
        SceneManager.LoadScene(MultiplayerSettings.multiplayerSetting.looserScene);
        Debug.Log("TESTING ###############");
        PhotonNetwork.DestroyPlayerObjects(PhotonNetwork.LocalPlayer);
        PhotonNetwork.LeaveRoom();

      }
    }


    /*
    This method is called via the network and triggers the testEnd method which
    will decide which result screen to load.
    */
    public void notifyGameEnded(string winner){
      Debug.Log("GAME ENDED in: " + PhotonNetwork.LocalPlayer.NickName + " getroffen hat: " + winner);
      foreach (Player pl in photonPlayers)
      {
        if(pl.NickName == winner)
        {
          Debug.Log(pl.GetScore());
          if(PhotonNetwork.IsMasterClient)
          {
            if(pl.GetScore() >= scoreLimit)
            {
                Debug.Log("will end game for the master: " + PhotonNetwork.LocalPlayer.NickName);
                testEnd(string.Equals(PhotonNetwork.LocalPlayer.NickName, winner));
            }
          }
          else
          {
            //adapt score by one since new values are not yet available on the slave
            if(pl.GetScore()+1 >= scoreLimit)
            {
                Debug.Log("will end game for the slave: " + PhotonNetwork.LocalPlayer.NickName);
                testEnd(string.Equals(PhotonNetwork.LocalPlayer.NickName, winner));

            }
          }

        }
      }

    }

    /*
    This method is called to initialize the player disconnetion.
    */
    public void DisconnectPlayer(bool winner)
    {
      StartCoroutine(DisconnectAndLoad(winner));
    }

    /*
    As long as the player is not disconnected, this method tries to disconnect
    the player.
    */
    IEnumerator DisconnectAndLoad(bool winner)
    {
      string myName = PhotonNetwork.NickName;
      PhotonNetwork.Disconnect();
      while(PhotonNetwork.IsConnected)
      {
        yield return null;
      }

      if(winner)
      {
        SceneManager.LoadScene(MultiplayerSettings.multiplayerSetting.winnerScene);
      }
      else
      {
        SceneManager.LoadScene(MultiplayerSettings.multiplayerSetting.looserScene);
      }
    }

    /*
    This method is used to show the current score for debuging reasons.
    */
    public void showScoreboard()
    {
      Player[] playerList = PhotonNetwork.PlayerList;

      foreach (Player pl in photonPlayers)
      {
        Debug.Log("Player " + pl.NickName + " has Score: " + pl.GetScore());
      }
    }

    /*
    This method is called by the master client is all players are connected and
    the game scene can be loaded. It loads the game scene.
    */
    [PunRPC]
    private void RPC_LoadedGameScene()
    {
      playerInGame++;
      if(playerInGame == PhotonNetwork.PlayerList.Length)
      {
        PV.RPC("RPC_CreatePlayer", RpcTarget.All);
      }
      if(sceneryCreated == false)
      {
        sceneryCreated = true;
      }
    }

    /*
    This method creates a new player instance and adds it to the network.
    */
    [PunRPC]
    private void RPC_CreatePlayer()
    {
      PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PhotonNetworkPlayer"), new Vector3(0, 0, 0), Quaternion.identity, 0);
    }

    /*
    This method is called if a player hits the exit button while playing the game
    typically if there is a connection issue. it brings the player back to the menu scene
    and disconnects - triggers the other players to leave the game as there are
    only 2 players per game.
    */
    [PunRPC]
    private void RPC_EndWithProblem()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        SceneManager.LoadScene(MultiplayerSettings.multiplayerSetting.menuScene);
        PhotonNetwork.Disconnect();
    }

    /*
    This method is used by the master client to spawn chicken objects via the network.
    */
    [PunRPC]
    private void RPC_SpawnChicken()
    {
      if(PhotonNetwork.IsMasterClient)
      {
        GameObject chicken = PhotonNetwork.InstantiateSceneObject(Path.Combine("PhotonPrefabs", "PhotonNetworkChicken"), new Vector3(Random.Range(-0.7f, 0.7f), 0.55f, Random.Range(-0.7f, 0.7f)), Quaternion.identity, 0);
        objectIds.Add(chicken);
      }

    }

    /*
    This method is used by the master client to destroy the chicken and update the
    score of the player who hit the chicken. Additionally the master instantiates
    a particle system in the network to show a hit effect.
    */
    public void destroyChicken(string nickname)
    {
      Debug.Log("master destroys chicken");
      if(PhotonNetwork.IsMasterClient)
      {
        int ctr = 0;
        foreach (GameObject obj in objectIds)
        {
          PhotonView pv = (PhotonView)obj.GetComponent("PhotonView");

          if (pv.ViewID == objectToDestroy)
          {
              objectIds.RemoveAt(ctr);
              PhotonNetwork.Destroy(obj);
              GameObject stone = PhotonNetwork.InstantiateSceneObject(Path.Combine("PhotonPrefabs", "PhotonNetworkStoneBulletEffect"), obj.transform.position, Quaternion.identity, 0);
              nrOfChicken--;
              //update player score
              foreach (Player pl in photonPlayers)
              {
                if(pl.NickName == nickname)
                {
                  // save hits to player
                  Debug.Log("found player");
                  pl.AddScore(1);
                  notifyGameEnded(nickname);
                }
              }
          }
          ctr++;
        }
      }
      else
      {
        Debug.Log("I am not the master");
      }
    }


    /*
    This method destroys the chicken just like above but this method is available
    as Remote procedure call.
    */
    [PunRPC]
    private void RPC_DestroyChicken(string nickname)
    {
      if(PhotonNetwork.IsMasterClient)
      {
        int ctr = 0;
        foreach (GameObject obj in objectIds)
        {

          PhotonView pv = (PhotonView)obj.GetComponent("PhotonView");
          if (pv.ViewID == objectToDestroy)
          {
              objectIds.RemoveAt(ctr);
              PhotonNetwork.Destroy(obj);
              GameObject stone = PhotonNetwork.InstantiateSceneObject(Path.Combine("PhotonPrefabs", "PhotonNetworkStoneBulletEffect"), obj.transform.position, Quaternion.identity, 0);
              nrOfChicken--;
              //update player score
              foreach (Player pl in photonPlayers)
              {
                if(pl.NickName == nickname)
                {
                  // save hits to player
                  Debug.Log("found player");
                  pl.AddScore(1);
                  notifyGameEnded(nickname);
                }
              }
          }
          ctr++;
        }
      }
      else
      {
        Debug.Log("I am not the master");
      }
    }

}
