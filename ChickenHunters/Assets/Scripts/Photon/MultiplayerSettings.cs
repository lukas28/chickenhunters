﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplayerSettings : MonoBehaviour
{
    /*
    Declaring all the neccessary variables which will be set in the untior
    inspector.
    */
    public static MultiplayerSettings multiplayerSetting;
    public bool delayStart;
    public int maxPlayers;
    public int menuScene;
    public int multiplayerScene;
    public int resultScene;
    public int winnerScene;
    public int looserScene;

    /*
    In this awake functino this class is Initialized as a singleton.
    */
    private void Awake(){
      if(MultiplayerSettings.multiplayerSetting == null){
        MultiplayerSettings.multiplayerSetting = this;
      }else{
        if(MultiplayerSettings.multiplayerSetting != this){
          Destroy(this.gameObject);
        }
      }
      DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
