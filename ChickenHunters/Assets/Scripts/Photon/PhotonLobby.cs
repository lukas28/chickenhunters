﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;
using Photon.Pun;
using Photon.Realtime;

public class PhotonLobby : MonoBehaviourPunCallbacks
{

    /*
    Initializing all the neccessary variables
    */
    public static PhotonLobby lobby;
    public GameObject battleButton;
    public GameObject cancelButton;

    /*
    Creating a singleton from this class
    */
    private void Awake()
    {
        lobby = this; //creates the singleton, lives within the Main menu scene
    }

    /*
    Initializing the class variables
    */
    void Start()
    {
        PhotonNetwork.ConnectUsingSettings();  //Connects to Master photon server
        battleButton.GetComponentInChildren<Text>().text = "Start";
        cancelButton.SetActive(false);

    }

    /*
    If the game is started and the PhotonLobby singleton is initialized, the
    application tries to connect to the Photon Servers. Once this was successfull,
    this callback is called.
    */
    public override void OnConnectedToMaster()
    {
        Debug.Log("Player has connected to the Photon master server");
        battleButton.GetComponentInChildren<Text>().text = "Start";
        battleButton.SetActive(true);
        cancelButton.SetActive(false);
        PhotonNetwork.AutomaticallySyncScene = true;
        //OnBattleButtonClicked();
    }

    /*
    If the player clicks on Start game, this method is called. It basically changes
    the ui appearence and shows that the device is about to connect and tries to
    join a radom room
    */
    public void OnBattleButtonClicked()
    {
        Debug.Log("Battle Button was clicked");
        battleButton.GetComponentInChildren<Text>().text = "Connecting";
        battleButton.SetActive(true);
        cancelButton.SetActive(true);
        PhotonNetwork.JoinRandomRoom(null, 0);
    }

    /*
    If there is currently no open room of chicken hunters, this method is called.
    Now that there is no room available, the device itself calls the room initialization.
    */
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("Tried to join a random game but failed. There must be no open games available");
        CreateRoom();
    }

    /*
    Here the device tries to create a new room.
    */
    void CreateRoom()
    {
      Debug.Log("Trying to create a new room");
      int randomRoomName = Random.Range(0, 10000);
      RoomOptions roomOps = new RoomOptions()
      {
        IsVisible = true, IsOpen = true, MaxPlayers = (byte)MultiplayerSettings.multiplayerSetting.maxPlayers
      };
      PhotonNetwork.CreateRoom("Room" + randomRoomName, roomOps);
    }

    /*
    This callback is called if the creation of the room is not successfull.
    */
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Tried to create a new room but failed, there must already be a room with the same name");
        CreateRoom();
    }

    /*
    This method is called if the user cancels to connection process by pressing the
    cancel button 
    */
    public void OnCancelButtonClicked()
    {
        cancelButton.SetActive(false);
        battleButton.GetComponentInChildren<Text>().text = "Start Game";
        battleButton.SetActive(true);
        PhotonNetwork.LeaveRoom();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
