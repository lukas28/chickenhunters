﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Movement : MonoBehaviour
{

    /* Declaring the neccessary variables */
    private Rigidbody rigidbody;
    private float lastDirectionChangeTime;
    private readonly float directionChangeTime = 3f;
    private float characterVelocity = 0.1f;
    private Vector2 movementDirection;
    private Vector2 movementPerSecond;
    private float angle;
    private float newX;
    private float newY;

    /*
    Start method used to get the object and Initialize the current
    movement direction
    */
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        lastDirectionChangeTime = 0f;
        changeAngle();
    }


    /*
    Show the raycast if the gizmos option is activated in the game mode.
    Furthermore check if the chicken is about to leave the game field and if so,
    use the priciple incoming movement agnle is outgoing movement angle to keep
    the chicken on the game field
    If the chicken is about to hit a tree, a stone or another chicken, calculate
    a new movement direction and applly it to the chicken
    */
    void Update()
    {

      Vector3 fwd = transform.TransformDirection(Vector3.forward);
      Vector3 forward = transform.TransformDirection(Vector3.forward) * 1;
      Debug.DrawRay(transform.position, forward, Color.red);

      if (Physics.Raycast(transform.position, fwd, 0.05f))
      {
        changeAngle();
      }

      float angleY = transform.eulerAngles.y;
      float x = (float)Math.Sin(angleY * ((2*Math.PI)/360));
      float z = (float)Math.Cos(angleY * ((2*Math.PI)/360));

      transform.position = new Vector3(transform.position.x + (x*Time.deltaTime*characterVelocity), 0.55f, transform.position.z + (z * Time.deltaTime * characterVelocity));

       if(transform.position.x > 0.9 || transform.position.x < -0.9)
       {
         transform.eulerAngles = new Vector3(transform.eulerAngles.x, 360.0f-angleY, transform.eulerAngles.z);
       }

       if(transform.position.z > 0.9 || transform.position.z < -0.9)
       {
         transform.eulerAngles = new Vector3(transform.eulerAngles.x, 180.0f-angleY, transform.eulerAngles.z);
       }

    }

    /*
    This function randomly generates a new movement direction
    */
    void changeAngle()
    {
      transform.Rotate(0.0f, UnityEngine.Random.Range(0.0f, 360.0f), 0.0f, Space.World);
    }


  /*
  Fallback solution using collision instead of reaycast
  */
   private void OnCollisionEnter(Collision collision)
   {

       if (collision.gameObject.name.StartsWith("Tree") || collision.gameObject.name.StartsWith("Boundary") || collision.gameObject.name.StartsWith("PhotonNetworkChicken"))
       {
          Debug.Log("collision");
          lastDirectionChangeTime = 0f;
          changeAngle();
       }
   }

   /*
   Fallback solution if raycast is not working
   */
   private void OnTriggerEnter(Collider other)
   {
     Debug.Log("triggered");
     changeAngle();
   }

}
