﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundForest : MonoBehaviour
{
    /*
    Declare the neccessary variables
    */
    public AudioClip myAudio;
    AudioSource audioS;

    /*
    This script initialzes the background noise (birds).
    */
    void Start()
    {
      audioS = GetComponent<AudioSource>();
      audioS.PlayOneShot(myAudio, 0.7f);
      Debug.Log("should play sound");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
