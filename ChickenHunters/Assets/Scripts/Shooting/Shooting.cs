﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;

public class Shooting : MonoBehaviour, IVirtualButtonEventHandler
{
  /*
  Declare all the neccessary variables which are used to do the shooting logic.
  */
  public GameObject camera;
  public GameObject concreteHit;
  public GameObject grassHit;
  public GameObject roomController;
  public PhotonRoom room;

  public GameObject mun1;
  public GameObject mun2;
  public GameObject mun3;
  public GameObject mun4;
  public GameObject mun5;

  public GameObject reloadBadge;

  public AudioClip myAudio;
  public AudioClip reloadAudio;
  AudioSource audioS;

  private int shootCounter;

  /*
  As this class implements the IVirtualButtonEventHandler interface, this method is
  used to get the callback if the reload virtual button is pressed. If so, all the ui
  elements indicating the bullets left are active again and the shootCounter which
  is the logic threshold whether there is a bullet left or not is reset to 5.
  */
  public void OnButtonPressed(VirtualButtonBehaviour vb){
      if(shootCounter < 5)
      {
        audioS.PlayOneShot(reloadAudio, 0.7f);
        Debug.Log("Button2 pressed");
        Handheld.Vibrate();
        Handheld.Vibrate();
        Handheld.Vibrate();
        mun1.SetActive(true);
        mun2.SetActive(true);
        mun3.SetActive(true);
        mun4.SetActive(true);
        mun5.SetActive(true);
        shootCounter = 5;
      }
  }

  public void OnButtonReleased(VirtualButtonBehaviour vb){

  }

  /*
  This class should not be destroyed when a new scene is laoded.
  */
  void Awake(){
    DontDestroyOnLoad(this.gameObject);
  }

  /*
  This method is used to initialize all the variables and get the reload
  virtual button working by setting this class the the event handler
  */
  void Start()
  {
      Debug.Log("in reload start");
      //PV = GetComponent<PhotonView>();
      roomController = GameObject.Find("RoomController");
      room = roomController.GetComponent<PhotonRoom>();

      audioS = GetComponent<AudioSource>();
      //activeScene = SceneManager.GetActiveScene();
      shootCounter = 5;

      reloadBadge = GameObject.Find("ReloadBadge");

      GameObject virtualButtonObject = GameObject.Find("ReloadButton");
      virtualButtonObject.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
      if(virtualButtonObject != null){
        Debug.Log("Button registered");
      }else{
        Debug.Log("Button not registered");
      }
  }

  /*
  In this method, the reload badge (red dot) over the relead station is animated
  to constatly rotatate. Also if the user shoots, one of the ui bullet elements is
  getting invisible and the shootCounter is decreased.
  */
  void Update()
  {
      //move the reload badge
      if(reloadBadge != null)
      {
          reloadBadge.transform.Rotate(0.0f, 1.0f, 0.0f, Space.Self);
          if(reloadBadge.transform.position.y >= 0.6f)
          {
            reloadBadge.transform.position = new Vector3(reloadBadge.transform.position.x ,reloadBadge.transform.position.y - 0.02f,
            reloadBadge.transform.position.z);
          }
          else if(reloadBadge.transform.position.y <= 0.4f)
          {
            reloadBadge.transform.position = new Vector3(reloadBadge.transform.position.x ,reloadBadge.transform.position.y + 0.02f,
            reloadBadge.transform.position.z);
          }
      }

      //if(Input.touchCount > 0){
      if (Input.GetMouseButtonDown(0))
      {
        if (shootCounter > 0)
        {
          Debug.Log("Should Shoot");
          Handheld.Vibrate();
          if (shootCounter == 5)
          {
            mun5.SetActive(false);
          }
          else if (shootCounter == 4)
          {
            mun4.SetActive(false);
          }
          else if (shootCounter == 3)
          {
            mun3.SetActive(false);
          }
          else if (shootCounter ==2)
          {
            mun2.SetActive(false);
          }
          else if (shootCounter == 1)
          {
            mun1.SetActive(false);
          }
          shootCounter--;
          audioS.PlayOneShot(myAudio, 0.7f);
          Shoot();
        }
      }
  }

  /*
  This method is called everytime the user shoots. It checks wheter the shot
  hits a chicken via a raycast or not. If a chicken is hit the method triggers a
  remote procedure call which the game master uses to destroy the chicken and update
  the score. If the bullet hits a stone or ground some particle effects are presented.
  */
  public void Shoot()
  {
      Debug.Log("my player id is: " + PhotonNetwork.LocalPlayer);
      CameraDevice.Instance.SetFlashTorchMode(true);
      Debug.Log("In shoot");
      RaycastHit hit;

      if(Physics.Raycast(camera.transform.position, camera.transform.forward, out hit))
      {
          CameraDevice.Instance.SetFlashTorchMode(false);
          if(hit.transform.name.StartsWith("PhotonNetworkChicken") || hit.transform.name.StartsWith("PhotonNetworkStone"))
          {
              room.PV.RPC("hitChicken", RpcTarget.All, ((PhotonView)hit.transform.gameObject.GetComponent("PhotonView")).ViewID, PhotonNetwork.LocalPlayer.NickName);
          }
          else if(hit.transform.name == "Grass")
          {
            GameObject go = Instantiate(grassHit, hit.point, Quaternion.LookRotation(hit.normal));
            room.PV.RPC("hitGrass", RpcTarget.All, hit.point);
          }
          else
          {
              Debug.Log("Kein Treffer");
          }
      }
      else
      {
          CameraDevice.Instance.SetFlashTorchMode(false);
          Debug.Log("Kein Treffer-no ray");
      }


  }

  /*
  This method is called if the user presses the exit button which calls a
  remote procedure call to end the game via the network.
  */
  public void endDueToProblem(){
    room.PV.RPC("RPC_EndWithProblem", RpcTarget.All);
  }
}
